#!/usr/bin/env bash

if [ ! -d London-wards-2018_ESRI ]; then
    wget https://data.london.gov.uk/download/statistical-gis-boundary-files-london/08d31995-dd27-423c-a987-57fe8e952990/London-wards-2018.zip
    unzip London-wards-2018.zip
fi
