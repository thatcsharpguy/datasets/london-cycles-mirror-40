#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import geopandas as gpd
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from datetime import datetime
from matplotlib.animation import FuncAnimation


# In[ ]:


shapefile = gpd.read_file("London-wards-2018_ESRI/London_Ward.shp").to_crs(epsg=4326)


# In[ ]:


execution_time = datetime.now()
stats_file_name = "data/stats-" + execution_time.date().strftime("%m-%d-%Y") + ".csv"


# In[ ]:


stations = pd.read_csv("data/stations.csv")
stats = pd.read_csv(stats_file_name)


# In[ ]:


stats['proportion'] = (stats['docks'] - stats['empty_docks']) / stats['docks']


# In[ ]:


min_y = stations["lat"].min() - 0.005
max_y = stations["lat"].max() + 0.005

min_x = stations["lon"].min() - 0.005
max_x = stations["lon"].max() + 0.005


# In[ ]:


unique_date_times = stats['query_time'].unique()


# In[ ]:


unique_date_times.sort()
unique_date_times


# In[ ]:


def create_frame(step, ax, fig):
    ax.cla()
    print(step)
    selected_date = unique_date_times[step]
    stats_for_date = stats[stats['query_time'] == selected_date].merge(stations)
    
    shapefile.plot(ax=ax)
    ax.set_ylim((min_y, max_y))
    ax.set_xlim((min_x, max_x))
    
    
    sns.scatterplot(y='lat', x='lon', hue='proportion', palette="Blues",data=stats_for_date, ax=ax, s=35)

    ax.axis('off')
    ax.get_legend().remove()
    ax.set_title("London cycles - " + selected_date)
    
    
    fig.tight_layout()
    fig.patch.set_facecolor('white')


# In[ ]:


fig = plt.figure(figsize=(6,4),dpi=170)
ax = fig.gca()
animation = FuncAnimation(fig, create_frame, frames=len(unique_date_times), fargs=(ax, fig))


# In[ ]:


from IPython.display import HTML
# .gif necesita la librería imagemagick que puede ser instalada con !apt install imagemagick
# animation.save('poly.gif', writer='imagemagick', fps=20); 
#animation.save('animation.mp4', writer='ffmpeg', fps=20);
HTML(animation.to_jshtml())


# In[ ]:


stats[['query_time', 'proportion']].groupby([stats.index, 'query_time']).head()


# In[ ]:


fig = plt.figure(figsize=(12,8),dpi=170)
ax = fig.gca()
shapefile.plot(ax=ax)
ax.set_ylim((min_y, max_y))
ax.set_xlim((min_x, max_x))

sns.scatterplot(y='lat', x='lon', hue='proportion', palette="Blues",data=stations_df, ax=ax, s=35)

ax.axis('off')
ax.get_legend().remove()
ax.set_title("London cycles - " + query_time.strftime("%m/%d/%Y, %H:%M:%S"))
fig.tight_layout()
fig.patch.set_facecolor('white')
fig.savefig('cycles.png')


# In[ ]:




